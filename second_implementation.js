my_array = [1, 7, 9, 0, 2, 8, 5, 4, 6]

const pair_is_in_array = (array_, pair) => {
  for (let pair_in_array of array_) {
    if ((pair[0] == pair_in_array[0] && pair[1] == pair_in_array[1]) ||
      (pair[0] == pair_in_array[1] && pair[1] == pair_in_array[0])) {
      return true
    }
  }
  return false
}


const find_pairs_that_sum_14 = (array_) => {
  found = []
  for (let i = 0; i < array_.length; i++) {
    for (let j = 0; j < array_.length; j++) {
      if (i == j) {
        continue
      }
      first_number = array_[i]
      second_number = array_[j]
      if (pair_is_in_array(found, [first_number, second_number])) {
        continue
      }
      if (first_number + second_number == 14) {
        found.push([first_number, second_number])
        console.log(array_[i], array_[j])
      }
    }
  }
}
find_pairs_that_sum_14(my_array)


// Output
// ~/projects/splunk-codechallenge 
// ✔  node second_implementation.js                                                         ✔
// 9 5
// 8 6