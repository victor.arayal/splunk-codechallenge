my_array = [1, 7, 9, 0, 2, 8, 5, 4, 6]

for (let i = 0; i < my_array.length; i++) {
  for (let j = 0; j < my_array.length; j++) {
    if (i == j) {
      continue
    }
    if (my_array[i] + my_array[j] == 14) {
      console.log(my_array[i], my_array[j])
      continue
    }
  }
}


// Output
// ~/projects/splunk-codechallenge 
// ✔  node first_implementation.js                                                          ✔
// 9 5
// 8 6
// 5 9
// 6 8